# ABO writer

[![pipeline status](https://gitlab.com/ales.krajnik/abo/badges/master/pipeline.svg)](https://gitlab.com/ales.krajnik/abo/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/064ab2de56e3433ea857efba641d34ce)](https://www.codacy.com/app/craynic/abo)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/064ab2de56e3433ea857efba641d34ce)](https://www.codacy.com/app/craynic/abo)

Writer for payments/collections orders in ABO format.
