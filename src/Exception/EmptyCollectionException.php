<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class EmptyCollectionException extends Exception
{
    public function __construct()
    {
        parent::__construct('Collection must not be empty.');
    }
}
