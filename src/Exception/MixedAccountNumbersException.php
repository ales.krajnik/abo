<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class MixedAccountNumbersException extends Exception
{
    public function __construct()
    {
        parent::__construct('All items must have submitter account numbers set or all items must have none.');
    }
}
