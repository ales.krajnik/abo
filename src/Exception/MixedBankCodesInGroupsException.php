<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class MixedBankCodesInGroupsException extends Exception
{
    public function __construct()
    {
        parent::__construct('Submitters in all groups have to have the same bank code.');
    }
}
