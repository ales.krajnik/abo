<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class Exception extends \Exception
{
}
