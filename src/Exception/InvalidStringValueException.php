<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

use Craynic\Abo\ValueObject\StringValue;

class InvalidStringValueException extends Exception
{
    public function __construct(StringValue $stringValue)
    {
        parent::__construct(
            sprintf(
                'Invalid string value "%s" of class %s.',
                (string) $stringValue,
                get_class($stringValue)
            )
        );
    }
}
