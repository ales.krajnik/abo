<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class InvalidAccountNumberStringException extends Exception
{
    public function __construct(string $accountNumber)
    {
        parent::__construct(
            sprintf('Invalid account number "%s".', $accountNumber)
        );
    }
}
