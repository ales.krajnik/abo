<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class MixedBankCodesException extends Exception
{
    public function __construct()
    {
        parent::__construct('All submitter account numbers must have the same bank code.');
    }
}
