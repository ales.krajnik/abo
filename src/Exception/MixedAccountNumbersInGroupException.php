<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

class MixedAccountNumbersInGroupException extends Exception
{
    public function __construct()
    {
        parent::__construct('Submitter account number must be set either in header or in all items, but not in both.');
    }
}
