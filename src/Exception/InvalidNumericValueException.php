<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

use Craynic\Abo\ValueObject\NumericValue;

class InvalidNumericValueException extends Exception
{
    public function __construct(NumericValue $numericValue)
    {
        parent::__construct(
            sprintf(
                'Invalid numeric value "%s" of class %s.',
                (string) $numericValue,
                get_class($numericValue)
            )
        );
    }
}
