<?php
declare(strict_types=1);

namespace Craynic\Abo\Exception;

use Craynic\Abo\ValueObject\AccountingItemNotes;

class TooManyNotesException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            sprintf('Too many notes - the maximum number is %d.', AccountingItemNotes::MAXIMUM_COUNT)
        );
    }
}
