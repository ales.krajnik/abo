<?php
declare(strict_types=1);

namespace Craynic\Abo;

use Craynic\Abo\ValueObject\AccountingFile;
use Craynic\Abo\ValueObject\AccountingFiles;
use Craynic\Abo\ValueObject\AccountingGroup;
use Craynic\Abo\ValueObject\AccountingGroups;
use Craynic\Abo\ValueObject\AccountingItem;
use Craynic\Abo\ValueObject\AccountingItemNote;
use Craynic\Abo\ValueObject\AccountingItemNotes;
use Craynic\Abo\ValueObject\AccountingItems;
use Craynic\Abo\ValueObject\AccountNumber;
use Craynic\Abo\ValueObject\ClientId;
use Craynic\Abo\ValueObject\ClientName;
use Craynic\Abo\ValueObject\ConstantSymbol;
use Craynic\Abo\ValueObject\DataFile;
use Craynic\Abo\ValueObject\DataFileHeader;
use Craynic\Abo\ValueObject\NumericValue;
use Craynic\Abo\ValueObject\SpecificSymbol;
use Craynic\Abo\ValueObject\VariableSymbol;

final class PaymentOrderWriter
{
    public function render(DataFile $dataFile): string
    {
        return $this->renderUhl1($dataFile->getHeader())
            . $this->renderAccountingFiles($dataFile->getAccountingFiles());
    }

    private function renderUhl1(DataFileHeader $header): string
    {
        return $this->renderLine(
            [
                $this->renderValue('UHL1', 4),
                $header->getDateTime()->format('dmy'),
                $this->renderValue((string) $header->getClientName(), ClientName::MAX_LENGTH),
                $this->renderValue((string) $header->getClientId(), ClientId::MAX_DIGITS, '0', \STR_PAD_LEFT),
                $this->renderNumericValue($header->getMinFileId()),
                $this->renderNumericValue($header->getMaxFileId()),
                $header->getDataSecurityCode()
                    ? $this->renderNumericValue($header->getDataSecurityCode()->getFixedPart())
                    : '',
                $header->getDataSecurityCode()
                    ? $this->renderNumericValue($header->getDataSecurityCode()->getSecretPart())
                    : ''
            ],
            ''
        );
    }

    private function renderLine(array $values, string $glue = ' '): string
    {
        return implode($glue, $values) . "\r\n";
    }

    private function renderValue(
        string $value,
        int $length,
        string $padCharacter = ' ',
        int $padType = \STR_PAD_RIGHT
    ): string
    {
        return str_pad($value, $length, $padCharacter, $padType);
    }

    private function renderNumericValue(NumericValue $value): string
    {
        return $this->renderValue((string) $value, $value::MAX_DIGITS, '0', \STR_PAD_LEFT);
    }

    private function renderAccountingFiles(AccountingFiles $accountingFiles): string
    {
        return implode(
            '',
            array_map(
                function (AccountingFile $accountingFile): string {
                    return $this->renderAccountingFile($accountingFile);
                },
                $accountingFiles->getItems()
            )
        );
    }

    private function renderAccountingFile(AccountingFile $accountingFile): string
    {
        return $this->renderHSO($accountingFile)
            . $this->renderAccountingGroups($accountingFile->getGroups())
            . $this->renderKSO();
    }

    private function renderHSO(AccountingFile $accountingFile): string
    {
        return $this->renderLine([
            '1',
            $this->renderValue((string)$accountingFile->getHeader()->getDataType()->getValue(), 4),
            $this->renderNumericValue($accountingFile->getHeader()->getId()) . '000',
            $this->renderNumericValue($accountingFile->getGroups()->getSubmitterBankCode())
        ]);
    }

    private function renderKSO(): string
    {
        return $this->renderLine(['5', '+']);
    }

    private function renderAccountingGroups(AccountingGroups $accountingGroups): string
    {
        return implode(
            '',
            array_map(
                function (AccountingGroup $accountingGroup): string {
                    return $this->renderAccountingGroup($accountingGroup);
                },
                $accountingGroups->getItems()
            )
        );
    }

    private function renderAccountingGroup(AccountingGroup $accountingGroup): string
    {
        return $this->renderHSK($accountingGroup)
            . $this->renderAccountingItems(
                $accountingGroup->getAccountingItems()
            )
            . $this->renderKSK();
    }

    private function renderHSK(AccountingGroup $accountingGroup): string
    {
        return $this->renderLine([
            '2',
            (string) $this->renderAccountNumberWithoutBankCode(
                $accountingGroup->getHeader()->getSubmitterAccountNumber()
            ),
            $accountingGroup->getAccountingItems()->getTotal()->getAmount(),
            $accountingGroup->getHeader()->getDueDate()->format('dmy'),
        ]);
    }

    private function renderAccountingItems(AccountingItems $accountingItems) : string
    {
        return implode(
            '',
            array_map(
                function (AccountingItem $accountingItem) : string {
                    return $this->renderAccountingItem($accountingItem);
                },
                $accountingItems->getItems()
            )
        );
    }

    private function renderAccountingItem(AccountingItem $accountingItem) : string
    {
        $items = [];

        if ($accountingItem->getSubmitterAccountNumber() !== null) {
            $items[] = $this->renderAccountNumberWithoutBankCode($accountingItem->getSubmitterAccountNumber());
        }

        $items = array_merge(
            $items,
            [
                $this->renderAccountNumberWithoutBankCode($accountingItem->getRecipientAccountNumber()),
                $accountingItem->getAmount()->getAmount(),
                $this->renderValue(
                    (string) $accountingItem->getVariableSymbol(),
                    VariableSymbol::MAX_LENGTH,
                    '0',
                    \STR_PAD_LEFT
                ),
                $this->renderNumericValue($accountingItem->getRecipientAccountNumber()->getBankCode())
                . $this->renderValue(
                    (string) $accountingItem->getConstantSymbol(),
                    ConstantSymbol::MAX_LENGTH,
                    '0',
                    \STR_PAD_LEFT
                ),
                $this->renderValue(
                    (string) $accountingItem->getSpecificSymbol(),
                    SpecificSymbol::MAX_LENGTH,
                    '0',
                    \STR_PAD_LEFT
                ),
                $this->renderAccountingItemNotes($accountingItem->getNotes())
            ]
        );

        return $this->renderLine($items);
    }

    private function renderAccountingItemNotes(?AccountingItemNotes $notes) : string
    {
        if (null === $notes) {
            return '';
        }

        return implode(
            '|',
            array_map(
                function (AccountingItemNote $note) : string {
                    return (string) $note;
                },
                \iterator_to_array($notes)
            )
        );
    }

    private function renderKSK(): string
    {
        return $this->renderLine(['3', '+']);
    }

    private function renderAccountNumberWithoutBankCode(?AccountNumber $accountNumber) : string
    {
        if ($accountNumber === null) {
            return '';
        }

        $out = '';

        if ($accountNumber->getPrefix() !== null) {
            $out .= $accountNumber->getPrefix() . '-';
        }

        $out .= $accountNumber->getAccount();

        return $out;
    }
}
