<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class AccountingFiles extends ImmutableCollection
{
    public function __construct(AccountingFile ... $files)
    {
        parent::__construct($files);
    }
}