<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class DataSecurityCode
{
    /** @var DataSecurityCodePart */
    private $fixedPart;

    /** @var DataSecurityCodePart */
    private $secretPart;

    public function __construct(DataSecurityCodePart $fixedPart, DataSecurityCodePart $secretPart)
    {
        $this->fixedPart = $fixedPart;
        $this->secretPart = $secretPart;
    }

    public function getFixedPart(): DataSecurityCodePart
    {
        return $this->fixedPart;
    }

    public function getSecretPart(): DataSecurityCodePart
    {
        return $this->secretPart;
    }
}
