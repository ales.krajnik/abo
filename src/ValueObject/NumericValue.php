<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\InvalidNumericValueException;

abstract class NumericValue
{
    public const MAX_DIGITS = null;

    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;

        $this->validate();
    }

    public function __toString() : string
    {
        return $this->value;
    }

    public function equals(NumericValue $otherValue): bool
    {
        return get_class($this) === get_class($otherValue)
            && $this->value === $otherValue->value;
    }

    protected function validate() : void
    {
        $validationRegExp = '|^[0-9]{0,' . (static::MAX_DIGITS ?: '') . '}$|';

        if (!preg_match($validationRegExp, $this->value)) {
            throw new InvalidNumericValueException($this);
        }
    }
}
