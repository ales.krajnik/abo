<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class SpecificSymbol extends NumericValue
{
    public const MAX_LENGTH = 10;
}
