<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\MixedAccountNumbersInGroupException;

class AccountingGroup
{
    /** @var AccountingGroupHeader */
    private $header;

    /** @var AccountingItems */
    private $accountingItems;

    public function __construct(AccountingGroupHeader $header, AccountingItems $accountingItems)
    {
        $this->header = $header;
        $this->accountingItems = $accountingItems;

        $this->validate();
    }

    public function getHeader(): AccountingGroupHeader
    {
        return $this->header;
    }

    /**
     * @return AccountingItems|AccountingItem[]
     */
    public function getAccountingItems(): AccountingItems
    {
        return $this->accountingItems;
    }

    public function getSubmitterBankCode(): BankCode
    {
        return $this->header->getSubmitterAccountNumber() !== null
            ? $this->header->getSubmitterAccountNumber()->getBankCode()
            : $this->getAccountingItems()->getSubmitterBankCode();
    }

    private function validate(): void
    {
        $this->validateSubmitterAccountNumber();
    }

    private function validateSubmitterAccountNumber(): void
    {
        $isAccNumSetInHeader = $this->header->getSubmitterAccountNumber() !== null;

        foreach ($this->getAccountingItems() as $accountingItem) {
            if (!($isAccNumSetInHeader xor ($accountingItem->getSubmitterAccountNumber() !== null))) {
                throw new MixedAccountNumbersInGroupException();
            }
        }
    }
}