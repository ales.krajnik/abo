<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class DataFileHeader
{
    /** @var \DateTimeImmutable */
    private $dateTime;

    /** @var ClientName|null */
    private $clientName;

    /** @var ClientId|null */
    private $clientId;

    /** @var AccountingFileId */
    private $minFileId;

    /** @var AccountingFileId */
    private $maxFileId;

    /** @var DataSecurityCode|null */
    private $dataSecurityCode;

    public function __construct(
        \DateTimeImmutable $dateTime = null,
        ClientName $clientName = null,
        ClientId $clientId = null,
        AccountingFileId $minFileId = null,
        AccountingFileId $maxFileId = null,
        DataSecurityCode $dataSecurityCode = null
    ) {
        $this->dateTime = $dateTime ?: new \DateTimeImmutable();
        $this->clientName = $clientName;
        $this->clientId = $clientId;
        $this->minFileId = $minFileId ?: AccountingFileId::min();
        $this->maxFileId = $maxFileId ?: AccountingFileId::max();
        $this->dataSecurityCode = $dataSecurityCode;
    }

    public function getDateTime(): \DateTimeImmutable
    {
        return $this->dateTime;
    }

    public function getClientName(): ?ClientName
    {
        return $this->clientName;
    }

    public function getClientId(): ?ClientId
    {
        return $this->clientId;
    }

    public function getMinFileId(): AccountingFileId
    {
        return $this->minFileId;
    }

    public function getMaxFileId(): AccountingFileId
    {
        return $this->maxFileId;
    }

    public function getDataSecurityCode(): ?DataSecurityCode
    {
        return $this->dataSecurityCode;
    }
}