<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class AccountingGroupHeader
{
    /** @var AccountNumber|null */
    private $submitterAccNumber;

    /** @var \DateTimeImmutable */
    private $dueDate;

    public function __construct(?AccountNumber $submitterAccNumber, \DateTimeImmutable $dueDate)
    {
        $this->submitterAccNumber = $submitterAccNumber;
        $this->dueDate = $dueDate;
    }

    public function getSubmitterAccountNumber(): ?AccountNumber
    {
        return $this->submitterAccNumber;
    }

    public function getDueDate(): \DateTimeImmutable
    {
        return $this->dueDate;
    }
}
