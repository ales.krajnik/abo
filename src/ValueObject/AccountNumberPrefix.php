<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class AccountNumberPrefix extends NumericValue
{
    public const MAX_DIGITS = 6;
}
