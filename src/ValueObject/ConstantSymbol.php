<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class ConstantSymbol extends NumericValue
{
    public const MAX_LENGTH = 4;
}
