<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class AccountNumberAccount extends NumericValue
{
    public const MAX_DIGITS = 10;
}
