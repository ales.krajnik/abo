<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class AccountingFileHeader
{
    /** @var AccountingFileId */
    private $id;

    /** @var AccountingFileDataType */
    private $dataType;

    public function __construct(AccountingFileId $id, AccountingFileDataType $dataType)
    {
        $this->id = $id;
        $this->dataType = $dataType;
    }

    public function getId(): AccountingFileId
    {
        return $this->id;
    }

    public function getDataType(): AccountingFileDataType
    {
        return $this->dataType;
    }
}
