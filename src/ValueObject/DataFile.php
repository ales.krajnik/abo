<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class DataFile
{
    /** @var DataFileHeader */
    private $header;

    /** @var AccountingFiles */
    private $accountingFiles;

    public function __construct(DataFileHeader $header, AccountingFiles $accountingFiles)
    {
        $this->header = $header;
        $this->accountingFiles = $accountingFiles;
    }

    public function getHeader(): DataFileHeader
    {
        return $this->header;
    }

    public function getAccountingFiles(): AccountingFiles
    {
        return $this->accountingFiles;
    }
}