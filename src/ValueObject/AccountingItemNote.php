<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class AccountingItemNote extends StringValue
{
    public const MAX_LENGTH = 35;
    protected const VALIDATION_REGEXP = '|^[0-9A-Za-z\s.,-\_]{0,' . self::MAX_LENGTH . '}$|';
}