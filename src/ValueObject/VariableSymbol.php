<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class VariableSymbol extends NumericValue
{
    public const MAX_LENGTH = 10;
}
