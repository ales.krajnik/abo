<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\InvalidStringValueException;

abstract class StringValue
{
    protected const VALIDATION_REGEXP = null;

    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;

        $this->validate();
    }

    public function __toString() : string
    {
        return $this->value;
    }

    protected function validate() : void
    {
        if (null !== static::VALIDATION_REGEXP) {
            if (!preg_match(static::VALIDATION_REGEXP, $this->value)) {
                throw new InvalidStringValueException($this);
            }
        }
    }
}
