<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Money\Money;

class AccountingItem
{
    /** @var AccountNumber|null */
    private $submitterAccNumber;

    /** @var AccountNumber|null */
    private $recAccNumber;

    /** @var Money */
    private $amount;

    /** @var VariableSymbol|null */
    private $variableSymbol;

    /** @var ConstantSymbol|null */
    private $constantSymbol;

    /** @var SpecificSymbol|null */
    private $specificSymbol;

    /** @var AccountingItemNotes|null */
    private $notes;

    public function __construct(
        ?AccountNumber $submitterAccNumber,
        AccountNumber $recAccNumber,
        Money $amount,
        VariableSymbol $variableSymbol = null,
        ConstantSymbol $constantSymbol = null,
        SpecificSymbol $specificSymbol = null,
        AccountingItemNotes $notes = null
    ) {
        $this->submitterAccNumber = $submitterAccNumber;
        $this->recAccNumber = $recAccNumber;
        $this->amount = $amount;
        $this->variableSymbol = $variableSymbol;
        $this->constantSymbol = $constantSymbol;
        $this->specificSymbol = $specificSymbol;
        $this->notes = $notes;
    }

    public function getSubmitterAccountNumber() : ?AccountNumber
    {
        return $this->submitterAccNumber;
    }

    public function getRecipientAccountNumber() : AccountNumber
    {
        return $this->recAccNumber;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getVariableSymbol() : ?VariableSymbol
    {
        return $this->variableSymbol;
    }

    public function getConstantSymbol() : ?ConstantSymbol
    {
        return $this->constantSymbol;
    }

    public function getSpecificSymbol() : ?SpecificSymbol
    {
        return $this->specificSymbol;
    }

    public function getNotes() : ?AccountingItemNotes
    {
        return $this->notes;
    }
}
