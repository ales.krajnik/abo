<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class ClientName extends StringValue
{
    public const MAX_LENGTH = 20;
    protected const VALIDATION_REGEXP = '|^[A-Z0-9]{0,' . self::MAX_LENGTH . '}$|';
}
