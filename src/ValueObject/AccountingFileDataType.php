<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use MabeEnum\Enum;

final class AccountingFileDataType extends Enum
{
    public const PAYMENT = 1501;
    public const COLLECTION = 1502;
}
