<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\InvalidAccountNumberStringException;

class AccountNumber
{
    /** @var AccountNumberPrefix|null */
    private $prefix;

    /** @var AccountNumberAccount */
    private $account;

    /** @var BankCode */
    private $bankCode;

    public function __construct(?AccountNumberPrefix $prefix, AccountNumberAccount $account, BankCode $bankCode)
    {
        $this->prefix = $prefix;
        $this->account = $account;
        $this->bankCode = $bankCode;
    }

    public static function fromString(string $accountNumber): AccountNumber
    {
        $subMatches = [];
        if (!preg_match('|^(?:(\d{1,6})-)?(\d{2,11})/(\d{4})$|', $accountNumber, $subMatches)) {
            throw new InvalidAccountNumberStringException($accountNumber);
        }

        return new static(
            $subMatches[1] == '' ? null : new AccountNumberPrefix(ltrim($subMatches[1], '0')),
            new AccountNumberAccount(ltrim($subMatches[2], '0')),
            new BankCode($subMatches[3])
        );
    }

    public function getPrefix(): ?AccountNumberPrefix
    {
        return $this->prefix;
    }

    public function getAccount(): AccountNumberAccount
    {
        return $this->account;
    }

    public function getBankCode(): BankCode
    {
        return $this->bankCode;
    }

    public function __toString(): string
    {
        $out = '';

        if ($this->getPrefix()) {
            $out .= $this->getPrefix() . '-';
        }

        $out .= $this->getAccount() . '/' . $this->getBankCode();

        return $out;
    }
}