<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class AccountingFileId extends NumericValue
{
    public const MAX_DIGITS = 3;

    public static function min() : AccountingFileId
    {
        return new AccountingFileId('1');
    }

    public static function max() : AccountingFileId
    {
        return new AccountingFileId(
            str_repeat('9', static::MAX_DIGITS)
        );
    }
}
