<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

final class ClientId extends NumericValue
{
    public const MAX_DIGITS = 10;
}
