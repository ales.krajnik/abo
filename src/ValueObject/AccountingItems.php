<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\MixedAccountNumbersException;
use Craynic\Abo\Exception\MixedBankCodesException;
use Money\Currency;
use Money\Money;

class AccountingItems extends ImmutableCollection
{
    public function __construct(AccountingItem ... $items)
    {
        parent::__construct($items);
    }

    public function getTotal(): Money
    {
        $total = new Money(0, new Currency(AccountingFile::CURRENCY));

        /** @var AccountingItem $item */
        foreach ($this as $item) {
            $total = $total->add($item->getAmount());
        }

        return $total;
    }

    public function getSubmitterBankCode() : ?BankCode
    {
        return $this->getFirstItem()->getSubmitterAccountNumber() !== null
            ? $this->getFirstItem()->getSubmitterAccountNumber()->getBankCode()
            : null;
    }

    public function getFirstItem(): AccountingItem
    {
        return parent::getFirstItem();
    }

    protected function validate(): void
    {
        parent::validate();

        $this->validateAccountNumbers();
    }

    private function validateAccountNumbers(): void
    {
        $uniqueBankCodes = array_unique(
            array_map(
                function (AccountingItem $item) : ?string {
                    return $item->getSubmitterAccountNumber() !== null
                        ? (string) $item->getSubmitterAccountNumber()->getBankCode()
                        : null;
                },
                \iterator_to_array($this)
            )
        );

        // the same value everywhere, that's ok
        if (count($uniqueBankCodes) == 1) {
            return;
        }

        // some of the values are NULL...
        if (array_search(null, $uniqueBankCodes, true)) {
            throw new MixedAccountNumbersException();
        } else {
            throw new MixedBankCodesException();
        }
    }
}
