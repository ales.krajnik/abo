<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\EmptyCollectionException;

class ImmutableCollection implements \IteratorAggregate, \Countable
{
    /** @var array */
    private $items;

    public function __construct(array $items)
    {
        $this->items = array_values($items);

        $this->validate();
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->items);
    }

    public function count() : int
    {
        return count($this->items);
    }

    protected function validate(): void
    {
        if (count($this) == 0) {
            throw new EmptyCollectionException();
        }
    }

    public function getFirstItem()
    {
        return $this->items[0];
    }
}