<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\TooManyNotesException;

class AccountingItemNotes extends ImmutableCollection
{
    public const MAXIMUM_COUNT = 4;

    public function __construct(AccountingItemNote ... $groups)
    {
        parent::__construct($groups);
    }

    public function getFirstItem(): AccountingItemNote
    {
        return parent::getFirstItem();
    }

    protected function validate(): void
    {
        parent::validate();

        if (count($this) > static::MAXIMUM_COUNT) {
            throw new TooManyNotesException();
        }
    }
}