<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

class AccountingFile
{
    public const CURRENCY = 'CZK';

    /** @var AccountingFileHeader */
    private $header;

    /** @var AccountingGroups */
    private $groups;

    public function __construct(AccountingFileHeader $header, AccountingGroups $groups)
    {
        $this->header = $header;
        $this->groups = $groups;
    }

    public function getHeader(): AccountingFileHeader
    {
        return $this->header;
    }

    public function getGroups(): AccountingGroups
    {
        return $this->groups;
    }
}