<?php
declare(strict_types=1);

namespace Craynic\Abo\ValueObject;

use Craynic\Abo\Exception\MixedBankCodesInGroupsException;

class AccountingGroups extends ImmutableCollection
{
    public function __construct(AccountingGroup ... $groups)
    {
        parent::__construct($groups);
    }

    public function getFirstItem(): AccountingGroup
    {
        return parent::getFirstItem();
    }

    public function getSubmitterBankCode(): BankCode
    {
        return $this->getFirstItem()->getSubmitterBankCode();
    }

    protected function validate(): void
    {
        parent::validate();

        $this->validateSameSubmitterBankCode();
    }

    private function validateSameSubmitterBankCode(): void
    {
        /** @var BankCode $bankCode */
        $bankCode = null;

        /** @var AccountingGroup $group */
        foreach ($this as $group) {
            if (null === $bankCode) {
                $bankCode = $group->getSubmitterBankCode();
            } elseif (!$bankCode->equals($group->getSubmitterBankCode())) {
                throw new MixedBankCodesInGroupsException();
            }
        }
    }
}