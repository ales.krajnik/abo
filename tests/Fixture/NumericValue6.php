<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\Fixture;

use Craynic\Abo\ValueObject\NumericValue;

class NumericValue6 extends NumericValue
{
    public const MAX_DIGITS = 6;
}
