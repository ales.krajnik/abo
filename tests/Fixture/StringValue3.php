<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\Fixture;

use Craynic\Abo\ValueObject\StringValue;

class StringValue3 extends StringValue
{
    protected const VALIDATION_REGEXP = '|^[a-z]{0,3}$|';
}
