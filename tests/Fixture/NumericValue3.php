<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\Fixture;

use Craynic\Abo\ValueObject\NumericValue;

class NumericValue3 extends NumericValue
{
    public const MAX_DIGITS = 3;
}
