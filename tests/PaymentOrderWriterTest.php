<?php
declare(strict_types=1);

namespace Craynic\Abo\Test;

use Craynic\Abo\ValueObject\AccountingFile;
use Craynic\Abo\ValueObject\AccountingFileDataType;
use Craynic\Abo\ValueObject\AccountingFileHeader;
use Craynic\Abo\ValueObject\AccountingFiles;
use Craynic\Abo\ValueObject\AccountingFileId;
use Craynic\Abo\ValueObject\AccountingGroup;
use Craynic\Abo\ValueObject\AccountingGroupHeader;
use Craynic\Abo\ValueObject\AccountingGroups;
use Craynic\Abo\ValueObject\AccountingItem;
use Craynic\Abo\ValueObject\AccountingItemNote;
use Craynic\Abo\ValueObject\AccountingItemNotes;
use Craynic\Abo\ValueObject\AccountingItems;
use Craynic\Abo\ValueObject\AccountNumber;
use Craynic\Abo\ValueObject\ClientId;
use Craynic\Abo\ValueObject\ClientName;
use Craynic\Abo\ValueObject\ConstantSymbol;
use Craynic\Abo\ValueObject\DataFile;
use Craynic\Abo\ValueObject\DataFileHeader;
use Craynic\Abo\ValueObject\DataSecurityCode;
use Craynic\Abo\ValueObject\DataSecurityCodePart;
use Craynic\Abo\ValueObject\SpecificSymbol;
use Craynic\Abo\ValueObject\VariableSymbol;
use Craynic\Abo\PaymentOrderWriter;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

final class PaymentOrderWriterTest extends TestCase
{
    public function testWrite() : void
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $file = new DataFile(
            new DataFileHeader(
                new \DateTimeImmutable('2016-12-31 23:59:59'),
                new ClientName('THISISCLIENTNAME'),
                new ClientId(''),
                AccountingFileId::min(),
                AccountingFileId::max(),
                new DataSecurityCode(new DataSecurityCodePart('111111'), new DataSecurityCodePart('222222'))
            ),
            new AccountingFiles(
                new AccountingFile(
                    new AccountingFileHeader(
                        new AccountingFileId('1'),
                        AccountingFileDataType::PAYMENT()
                    ),
                    new AccountingGroups(
                        new AccountingGroup(
                            new AccountingGroupHeader(
                                AccountNumber::fromString('123-123456789/1234'),
                                new \DateTimeImmutable('2017-01-07 23:59:59')
                            ),
                            new AccountingItems(
                                new AccountingItem(
                                    null,
                                    AccountNumber::fromString('456-987654321/4321'),
                                    new Money(111, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('1234567890'),
                                    new ConstantSymbol('0308'),
                                    new SpecificSymbol('9876543210'),
                                    new AccountingItemNotes(
                                        new AccountingItemNote('abcd'),
                                        new AccountingItemNote('efgh'),
                                        new AccountingItemNote('ijkl'),
                                        new AccountingItemNote('mnop')
                                    )
                                ),
                                new AccountingItem(
                                    null,
                                    AccountNumber::fromString('123123/1231'),
                                    new Money(222, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('11111111'),
                                    new ConstantSymbol('2222'),
                                    new SpecificSymbol('33333333')
                                ),
                                new AccountingItem(
                                    null,
                                    AccountNumber::fromString('321321/4444'),
                                    new Money(333, new Currency(AccountingFile::CURRENCY))
                                )
                            )
                        ),
                        new AccountingGroup(
                            new AccountingGroupHeader(
                                null,
                                new \DateTimeImmutable('2017-01-10 23:59:59')
                            ),
                            new AccountingItems(
                                new AccountingItem(
                                    AccountNumber::fromString('1111-11111111/1234'),
                                    AccountNumber::fromString('999-999999999/9999'),
                                    new Money(444, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('1234567890'),
                                    new ConstantSymbol('0308'),
                                    new SpecificSymbol('9876543210'),
                                    new AccountingItemNotes(
                                        new AccountingItemNote('ddddeeee')
                                    )
                                ),
                                new AccountingItem(
                                    AccountNumber::fromString('2222-22222222/1234'),
                                    AccountNumber::fromString('88888888/8888'),
                                    new Money(555, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('11111111'),
                                    new ConstantSymbol('2222'),
                                    new SpecificSymbol('33333333')
                                ),
                                new AccountingItem(
                                    AccountNumber::fromString('3333-33333333/1234'),
                                    AccountNumber::fromString('77777777/7777'),
                                    new Money(666, new Currency(AccountingFile::CURRENCY))
                                )
                            )
                        )
                    )
                ),
                new AccountingFile(
                    new AccountingFileHeader(
                        new AccountingFileId('2'),
                        AccountingFileDataType::COLLECTION()
                    ),
                    new AccountingGroups(
                        new AccountingGroup(
                            new AccountingGroupHeader(
                                AccountNumber::fromString('55-5555/1111'),
                                new \DateTimeImmutable('2017-01-21 23:59:59')
                            ),
                            new AccountingItems(
                                new AccountingItem(
                                    null,
                                    AccountNumber::fromString('66-666666/6666'),
                                    new Money(12345, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('123123'),
                                    new ConstantSymbol('123'),
                                    new SpecificSymbol('123123123'),
                                    new AccountingItemNotes(
                                        new AccountingItemNote('xxxx')
                                    )
                                ),
                                new AccountingItem(
                                    null,
                                    AccountNumber::fromString('7777/7777'),
                                    new Money(23456, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('7'),
                                    new ConstantSymbol('77'),
                                    new SpecificSymbol('777')
                                ),
                                new AccountingItem(
                                    null,
                                    AccountNumber::fromString('8888/8888'),
                                    new Money(34567, new Currency(AccountingFile::CURRENCY))
                                )
                            )
                        ),
                        new AccountingGroup(
                            new AccountingGroupHeader(
                                null,
                                new \DateTimeImmutable('2017-01-11 23:59:59')
                            ),
                            new AccountingItems(
                                new AccountingItem(
                                    AccountNumber::fromString('1-11/1111'),
                                    AccountNumber::fromString('2-22/2222'),
                                    new Money(1111, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('2222'),
                                    new ConstantSymbol('3333'),
                                    new SpecificSymbol('4444'),
                                    new AccountingItemNotes(
                                        new AccountingItemNote('ffff')
                                    )
                                ),
                                new AccountingItem(
                                    AccountNumber::fromString('3-33/1111'),
                                    AccountNumber::fromString('44/4444'),
                                    new Money(2222, new Currency(AccountingFile::CURRENCY)),
                                    new VariableSymbol('5555'),
                                    new ConstantSymbol('6666'),
                                    new SpecificSymbol('7777')
                                ),
                                new AccountingItem(
                                    AccountNumber::fromString('5-55/1111'),
                                    AccountNumber::fromString('66/6666'),
                                    new Money(3333, new Currency(AccountingFile::CURRENCY))
                                )
                            )
                        )
                    )
                )
            )
        );

        $writer = new PaymentOrderWriter();

        $expected =
            'UHL1311216THISISCLIENTNAME    0000000000001999111111222222' . "\r\n"
            . '1 1501 001000 1234' . "\r\n"
            . '2 123-123456789 666 070117' . "\r\n"
            . '456-987654321 111 1234567890 43210308 9876543210 abcd|efgh|ijkl|mnop' . "\r\n"
            . '123123 222 0011111111 12312222 0033333333 ' . "\r\n"
            . '321321 333 0000000000 44440000 0000000000 ' . "\r\n"
            . '3 +' . "\r\n"
            . '2  1665 100117' . "\r\n"
            . '1111-11111111 999-999999999 444 1234567890 99990308 9876543210 ddddeeee' . "\r\n"
            . '2222-22222222 88888888 555 0011111111 88882222 0033333333 ' . "\r\n"
            . '3333-33333333 77777777 666 0000000000 77770000 0000000000 ' . "\r\n"
            . '3 +' . "\r\n"
            . '5 +' . "\r\n"
            . '1 1502 002000 1111' . "\r\n"
            . '2 55-5555 70368 210117' . "\r\n"
            . '66-666666 12345 0000123123 66660123 0123123123 xxxx' . "\r\n"
            . '7777 23456 0000000007 77770077 0000000777 ' . "\r\n"
            . '8888 34567 0000000000 88880000 0000000000 ' . "\r\n"
            . '3 +' . "\r\n"
            . '2  6666 110117' . "\r\n"
            . '1-11 2-22 1111 0000002222 22223333 0000004444 ffff' . "\r\n"
            . '3-33 44 2222 0000005555 44446666 0000007777 ' . "\r\n"
            . '5-55 66 3333 0000000000 66660000 0000000000 ' . "\r\n"
            . '3 +' . "\r\n"
            . '5 +' . "\r\n";

        $this->assertSame($expected, $writer->render($file));
    }
}
