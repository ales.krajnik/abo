<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\ValueObject\AccountingItemNote;
use Craynic\Abo\ValueObject\AccountingItemNotes;
use Craynic\Abo\Exception\TooManyNotesException;
use PHPUnit\Framework\TestCase;

final class AccountingItemNotesTest extends TestCase
{
    public function testCanCreateNotesCollection() : void
    {
        $notes = new AccountingItemNotes(
            new AccountingItemNote('asdfasdf'),
            new AccountingItemNote('qwrweqrw'),
            new AccountingItemNote('qasfweqr')
        );

        $this->assertCount(3, $notes);
    }

    public function testTooManyNotesThrowsException() : void
    {
        $this->expectException(TooManyNotesException::class);

        new AccountingItemNotes(
            new AccountingItemNote('asdfasdf'),
            new AccountingItemNote('qwrweqrw'),
            new AccountingItemNote('qasfweqr'),
            new AccountingItemNote('qqqwqweq'),
            new AccountingItemNote('rrereeee')
        );
    }
}
