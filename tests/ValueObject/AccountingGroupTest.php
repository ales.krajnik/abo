<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\ValueObject\AccountingGroup;
use Craynic\Abo\ValueObject\AccountingGroupHeader;
use Craynic\Abo\ValueObject\AccountingItem;
use Craynic\Abo\ValueObject\AccountingItems;
use Craynic\Abo\ValueObject\AccountNumber;
use Craynic\Abo\ValueObject\BankCode;
use Craynic\Abo\Exception\MixedAccountNumbersInGroupException;
use PHPUnit\Framework\TestCase;

final class AccountingGroupTest extends TestCase
{
    public function testGetBankCodeWorksWhenAccountNumberIsSetInGroup() : void
    {
        $bankCode = '3333';
        $accountingItem1 = $this->getItemMock();
        $accountingItem2 = $this->getItemMock();
        $accountingItem3 = $this->getItemMock();
        $header = $this->getHeaderMock($this->getAccountNumberMock($bankCode));

        $items = new AccountingItems($accountingItem1, $accountingItem2, $accountingItem3);
        $group = new AccountingGroup($header, $items);

        $this->assertSame($bankCode, (string) $group->getSubmitterBankCode());
    }

    public function testGetBankCodeWorksWhenAccountNumbersAreSetInItems() : void
    {
        $bankCode = '3333';
        $accountingItem1 = $this->getItemMock($this->getAccountNumberMock($bankCode));
        $accountingItem2 = $this->getItemMock($this->getAccountNumberMock($bankCode));
        $accountingItem3 = $this->getItemMock($this->getAccountNumberMock($bankCode));
        $header = $this->getHeaderMock();

        $items = new AccountingItems($accountingItem1, $accountingItem2, $accountingItem3);
        $group = new AccountingGroup($header, $items);

        $this->assertSame($bankCode, (string) $group->getSubmitterBankCode());
    }

    public function testAccountingGroupsConstructorThrowsExceptionWhenAccountNumberIsNotSetAtAll() : void
    {
        $accountingItem1 = $this->getItemMock();
        $accountingItem2 = $this->getItemMock();
        $accountingItem3 = $this->getItemMock();
        $header = $this->getHeaderMock();

        $items = new AccountingItems($accountingItem1, $accountingItem2, $accountingItem3);

        $this->expectException(MixedAccountNumbersInGroupException::class);

        new AccountingGroup($header, $items);
    }

    public function testAccountingGroupsConstructorThrowsExceptionWhenAccountNumberIsSetInBothGroupAndItems() : void
    {
        $bankCode = '3333';

        $accountingItem1 = $this->getItemMock($this->getAccountNumberMock($bankCode));
        $accountingItem2 = $this->getItemMock($this->getAccountNumberMock($bankCode));
        $accountingItem3 = $this->getItemMock($this->getAccountNumberMock($bankCode));
        $header = $this->getHeaderMock($this->getAccountNumberMock($bankCode));

        $items = new AccountingItems($accountingItem1, $accountingItem2, $accountingItem3);

        $this->expectException(MixedAccountNumbersInGroupException::class);

        new AccountingGroup($header, $items);
    }

    private function getHeaderMock(AccountNumber $accountNumber = null) : AccountingGroupHeader
    {
        $header = $this->getMockBuilder(AccountingGroupHeader::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSubmitterAccountNumber'])
            ->getMock();
        $header
            ->method('getSubmitterAccountNumber')
            ->willReturn($accountNumber);

        /** @var AccountingGroupHeader $header */

        return $header;
    }

    private function getItemMock(AccountNumber $accountNumber = null) : AccountingItem
    {
        $header = $this->getMockBuilder(AccountingItem::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSubmitterAccountNumber'])
            ->getMock();
        $header
            ->method('getSubmitterAccountNumber')
            ->willReturn($accountNumber);

        /** @var AccountingItem $header */

        return $header;
    }

    private function getAccountNumberMock(string $bankCode = '1234') : AccountNumber
    {
        $accountNumber = $this->getMockBuilder(AccountNumber::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBankCode'])
            ->getMock();

        $accountNumber
            ->method('getBankCode')
            ->willReturn(new BankCode($bankCode));

        /** @var AccountNumber $accountNumber */

        return $accountNumber;
    }
}
