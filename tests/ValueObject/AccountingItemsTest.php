<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\ValueObject\AccountingFile;
use Craynic\Abo\ValueObject\AccountingItem;
use Craynic\Abo\ValueObject\AccountingItems;
use Craynic\Abo\ValueObject\AccountNumber;
use Craynic\Abo\ValueObject\BankCode;
use Craynic\Abo\Exception\MixedAccountNumbersException;
use Craynic\Abo\Exception\MixedBankCodesException;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

final class AccountingItemsTest extends TestCase
{
    public function testNoExceptionIsThrownWhenNoAccountNumberProvided() : void
    {
        $item1 = $this->getItemMock(null, 111);
        $item2 = $this->getItemMock(null, 222);
        $item3 = $this->getItemMock(null, 333);

        new AccountingItems($item1, $item2, $item3);

        $this->assertTrue(true);
    }

    public function testNoExceptionIsThrownWhenAllAccountNumbersAreProvidedWithTheSameBankCode() : void
    {
        $item1 = $this->getItemMock($this->getAccountNumberMock('1234'), 111);
        $item2 = $this->getItemMock($this->getAccountNumberMock('1234'), 222);
        $item3 = $this->getItemMock($this->getAccountNumberMock('1234'), 333);

        new AccountingItems($item1, $item2, $item3);

        $this->assertTrue(true);
    }

    public function testExceptionIsThrownWhenOnlySomeItemsHaveAccountNumber() : void
    {
        $item1 = $this->getItemMock($this->getAccountNumberMock('1234'), 111);
        $item2 = $this->getItemMock($this->getAccountNumberMock('1234'), 222);
        $item3 = $this->getItemMock(null, 333);

        $this->expectException(MixedAccountNumbersException::class);

        new AccountingItems($item1, $item2, $item3);
    }

    public function testExceptionIsThrownWhenAccountNumbersHaveDifferentBankCodes() : void
    {
        $item1 = $this->getItemMock($this->getAccountNumberMock('1234'), 111);
        $item2 = $this->getItemMock($this->getAccountNumberMock('1234'), 222);
        $item3 = $this->getItemMock($this->getAccountNumberMock('2345'), 333);

        $this->expectException(MixedBankCodesException::class);

        new AccountingItems($item1, $item2, $item3);
    }

    public function testGetTotalWorks() : void
    {
        $item1 = $this->getItemMock(null, 111);
        $item2 = $this->getItemMock(null, 222);
        $item3 = $this->getItemMock(null, 333);
        $items = new AccountingItems($item1, $item2, $item3);

        $this->assertEquals(new Money(666, new Currency(AccountingFile::CURRENCY)), $items->getTotal());
    }

    public function testGetSubmitterBankCodeReturnsNullWhenNoItemHasABankAccount() : void
    {
        $item1 = $this->getItemMock(null, 111);
        $item2 = $this->getItemMock(null, 222);
        $item3 = $this->getItemMock(null, 333);

        $items = new AccountingItems($item1, $item2, $item3);

        $this->assertNull($items->getSubmitterBankCode());
    }

    public function testGetSubmitterBankCodeReturnsBankCodeWhenAllItemsHaveTheSameBankCode() : void
    {
        $item1 = $this->getItemMock($this->getAccountNumberMock('1234'), 111);
        $item2 = $this->getItemMock($this->getAccountNumberMock('1234'), 222);
        $item3 = $this->getItemMock($this->getAccountNumberMock('1234'), 333);

        $items = new AccountingItems($item1, $item2, $item3);

        $this->assertEquals(new BankCode('1234'), $items->getSubmitterBankCode());
    }

    private function getItemMock(?AccountNumber $accountNumber, int $amount = 1) : AccountingItem
    {
        $accountingItem = $this->getMockBuilder(AccountingItem::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSubmitterAccountNumber', 'getAmount'])
            ->getMock();

        $accountingItem
            ->method('getSubmitterAccountNumber')
            ->willReturn($accountNumber);

        $accountingItem
            ->method('getAmount')
            ->willReturn(new Money($amount, new Currency(AccountingFile::CURRENCY)));

        /** @var AccountingItem $accountingItem */

        return $accountingItem;
    }

    private function getAccountNumberMock(string $bankCode = '1234') : AccountNumber
    {
        $accountNumber = $this->getMockBuilder(AccountNumber::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBankCode'])
            ->getMock();

        $accountNumber
            ->method('getBankCode')
            ->willReturn(new BankCode($bankCode));

        /** @var AccountNumber $accountNumber */

        return $accountNumber;
    }
}
