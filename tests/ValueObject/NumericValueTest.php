<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\Exception\InvalidNumericValueException;
use Craynic\Abo\Test\Fixture\NumericValue3;
use Craynic\Abo\Test\Fixture\NumericValue6;
use PHPUnit\Framework\TestCase;

final class NumericValueTest extends TestCase
{
    /**
     * @param string $invalidValue
     * @dataProvider invalidValueCausesExceptionDataProvider
     */
    public function testInvalidValueCausesException(string $invalidValue): void
    {
        $this->expectException(InvalidNumericValueException::class);

        new NumericValue3($invalidValue);
    }

    public function invalidValueCausesExceptionDataProvider(): array
    {
        return [
            'invalid chars 1' => ['ABC'],
            'invalid chars 2' => ['123ABC'],
            'invalid chars 3' => ['123 234'],
            'invalid chars 4' => ['123.123'],
            'invalid chars 5' => ['123,123'],
            'too long' => ['1234'],
        ];
    }

    public function testSameValuesEqual(): void
    {
        $value = '123';
        $numericValue1 = new NumericValue3($value);
        $numericValue2 = new NumericValue3($value);

        $this->assertTrue(
            $numericValue1->equals($numericValue2)
        );
    }

    public function testDifferentValuesDoNotEqual(): void
    {
        $numericValue1 = new NumericValue3('123');
        $numericValue2 = new NumericValue3('234');

        $this->assertFalse(
            $numericValue1->equals($numericValue2)
        );
    }

    public function testDifferentClassesDoNotEqual(): void
    {
        $numericValue1 = new NumericValue3('123');
        $numericValue2 = new NumericValue6('123');

        $this->assertFalse(
            $numericValue1->equals($numericValue2)
        );
    }
}
