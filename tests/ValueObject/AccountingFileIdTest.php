<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\ValueObject\AccountingFileId;
use PHPUnit\Framework\TestCase;

final class AccountingFileIdTest extends TestCase
{
    public function testGetMin() : void
    {
        $this->assertSame(
            '1',
            (string) AccountingFileId::min()
        );
    }
    public function testGetMax() : void
    {
        $this->assertSame(
            '999',
            (string) AccountingFileId::max()
        );
    }
}