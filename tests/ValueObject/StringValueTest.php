<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\Exception\InvalidStringValueException;
use Craynic\Abo\Test\Fixture\StringValue3;
use PHPUnit\Framework\TestCase;

final class StringValueTest extends TestCase
{
    /**
     * @param string $invalidValue
     * @dataProvider invalidValueCausesExceptionDataProvider
     */
    public function testInvalidValueCausesException(string $invalidValue): void
    {
        $this->expectException(InvalidStringValueException::class);

        new StringValue3($invalidValue);
    }

    public function invalidValueCausesExceptionDataProvider(): array
    {
        return [
            'invalid chars' => ['ABC'],
            'too long' => ['abcd'],
        ];
    }
}
