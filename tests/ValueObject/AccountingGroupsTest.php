<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\ValueObject\AccountingGroup;
use Craynic\Abo\ValueObject\AccountingGroups;
use Craynic\Abo\ValueObject\BankCode;
use Craynic\Abo\Exception\MixedBankCodesInGroupsException;
use PHPUnit\Framework\TestCase;

final class AccountingGroupsTest extends TestCase
{
    public function testGetSubmitterBankCodeWorksWhenBankCodesAreTheSameInAllGroups() : void
    {
        $bankCode = '4444';
        $group1 = $this->getGroupMock($bankCode);
        $group2 = $this->getGroupMock($bankCode);

        $groups = new AccountingGroups($group1, $group2);

        $this->assertSame($bankCode, (string) $groups->getSubmitterBankCode());
    }

    public function testMixedBankCodesThrowsException() : void
    {
        $group1 = $this->getGroupMock('4444');
        $group2 = $this->getGroupMock('5555');

        $this->expectException(MixedBankCodesInGroupsException::class);

        new AccountingGroups($group1, $group2);
    }

    private function getGroupMock(string $bankCode) : AccountingGroup
    {
        $group = $this->getMockBuilder(AccountingGroup::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSubmitterBankCode'])
            ->getMock();
        $group
            ->method('getSubmitterBankCode')
            ->willReturn(new BankCode($bankCode));

        /** @var AccountingGroup $group */

        return $group;
    }
}
