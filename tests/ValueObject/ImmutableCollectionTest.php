<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\Exception\EmptyCollectionException;
use Craynic\Abo\ValueObject\ImmutableCollection;
use PHPUnit\Framework\TestCase;

final class ImmutableCollectionTest extends TestCase
{
    public function testGetItems() : void
    {
        $items = [
            '123',
            '234',
            '345',
        ];

        $this->assertSame(
            array_values($items),
            (new ImmutableCollection($items))->getItems()
        );
    }

    public function testCount() : void
    {
        $items = [
            '123',
            '234',
            '345',
        ];

        $this->assertCount(
            count($items),
            new ImmutableCollection($items)
        );
    }

    public function testEmptyCollectionThrowsException() : void
    {
        $this->expectException(EmptyCollectionException::class);

        new ImmutableCollection([]);
    }

    public function testGetFirstItem() : void
    {
        $firstItem = '123';

        $items = [
            $firstItem,
            '234',
            '345',
        ];

        $this->assertSame(
            $firstItem,
            (new ImmutableCollection($items))->getFirstItem()
        );
    }
}