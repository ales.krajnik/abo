<?php
declare(strict_types=1);

namespace Craynic\Abo\Test\ValueObject;

use Craynic\Abo\ValueObject\AccountNumber;
use Craynic\Abo\ValueObject\AccountNumberAccount;
use Craynic\Abo\ValueObject\AccountNumberPrefix;
use Craynic\Abo\ValueObject\BankCode;
use Craynic\Abo\Exception\InvalidAccountNumberStringException;
use PHPUnit\Framework\TestCase;

class AccountNumberTest extends TestCase
{
    /**
     * @param string $accountNumberString
     * @param AccountNumber $expectedNumber
     * @dataProvider createFromStringWorkDataProvider
     */
    public function testCreateFromStringWork(string $accountNumberString, AccountNumber $expectedNumber): void
    {
        $accountNumber = AccountNumber::fromString($accountNumberString);

        $this->assertInstanceOf(AccountNumber::class, $accountNumber, $this->getName());
        $this->assertSame(
            (string) $expectedNumber->getPrefix(),
            (string) $accountNumber->getPrefix(),
            'Wrong account prefix in ' . $this->getName()
        );
        $this->assertSame(
            (string) $expectedNumber->getAccount(),
            (string) $accountNumber->getAccount(),
            'Wrong account number in ' . $this->getName()
        );
        $this->assertSame(
            (string) $expectedNumber->getBankCode(),
            (string) $accountNumber->getBankCode(),
            'Wrong bank code in ' . $this->getName()
        );
    }

    public function createFromStringWorkDataProvider(): array
    {
        return [
            'without prefix' => [
                '123456789/1234',
                new AccountNumber(
                    null,
                    new AccountNumberAccount('123456789'),
                    new BankCode('1234')
                ),
            ],
            'with prefix' => [
                '9876-123456789/1234',
                new AccountNumber(
                    new AccountNumberPrefix('9876'),
                    new AccountNumberAccount('123456789'),
                    new BankCode('1234')
                ),
            ],
            'with prefix and leading zeroes' => [
                '0876-003456789/0034',
                new AccountNumber(
                    new AccountNumberPrefix('876'),
                    new AccountNumberAccount('3456789'),
                    new BankCode('0034')
                ),
            ],
        ];
    }

    /**
     * @param string $accountNumberString
     * @dataProvider wrongAccountNumberThrowsExceptionDataProvider
     */
    public function testWrongAccountNumberThrowsException(string $accountNumberString): void
    {
        $this->expectException(InvalidAccountNumberStringException::class);

        AccountNumber::fromString($accountNumberString);
    }

    public function wrongAccountNumberThrowsExceptionDataProvider(): array
    {
        return [
            'empty prefix' => [
                '-123/1234',
            ],
            'too long prefix' => [
                '1234567-123/1234',
            ],
            'too short account number' => [
                '123-1/1234',
            ],
            'too long account number' => [
                '123-123456789012/1234',
            ],
            'too short bank code' => [
                '123-123456789012/123',
            ],
            'too long bank code' => [
                '123-123456789012/12345',
            ],
            'non-numeric chars in prefix' => [
                'a-123456789/1234',
            ],
            'non-numeric chars in account number' => [
                '123-a23456789/1234',
            ],
            'non-numeric chars in bank code' => [
                '123-123456789/123a',
            ],
        ];
    }
}